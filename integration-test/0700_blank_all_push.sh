#!/bin/bash
set -e
echo $0
rm -rf blankproject
mkdir blankproject
cd blankproject
../node_modules/featbuckawsmobile-cli/bin/featbuckawsmobile configure aws $1 $2 us-east-1
../node_modules/featbuckawsmobile-cli/bin/featbuckawsmobile init -y
../node_modules/featbuckawsmobile-cli/bin/featbuckawsmobile user-signin enable
../node_modules/featbuckawsmobile-cli/bin/featbuckawsmobile user-files enable
../node_modules/featbuckawsmobile-cli/bin/featbuckawsmobile cloud-api enable
../node_modules/featbuckawsmobile-cli/bin/featbuckawsmobile database enable
../node_modules/featbuckawsmobile-cli/bin/featbuckawsmobile analytics enable
../node_modules/featbuckawsmobile-cli/bin/featbuckawsmobile hosting enable
../node_modules/featbuckawsmobile-cli/bin/featbuckawsmobile push
../node_modules/featbuckawsmobile-cli/bin/featbuckawsmobile delete -f
cd ..
rm -rf blankproject
echo end